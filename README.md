# cpp_projects



## General Introduction

This repo contains the only two open-source projects I made in C++.
I did this project during my Ph.D. and my first postdoc, so they follow an academic coding standard far from industrial requirements.\
Unfortunately, the big projects I did in C++ in the industry, such as a CAN bus real-time simulator or a sequencer for PCB analyzer device, are the company's property.
I am, therefore, not allowed to disclose these works.\
I hope the other ones are enough to demonstrate my knowledge of the C++ language.\
I also taught object-oriented programming and C++ to undergrad students at the University of Montpellier. However, part of the materials are in french, so I decided not to include that in this repo. 


## Getting started

To fetch this repository:

```
git clone --recursive origin https://gitlab.com/mfrancep/cpp_projects.git
```

## Repo description

- profiling_toolchain: this directory contains the profiling toolchain designed during my Ph.D. A detailed presentation of it is available [here](https://dl.acm.org/doi/10.1145/3445030).\
Note: I recovered this project from old archives. So, I lost the commit history.

- NoC_Simulator: this directory contains my contribution to the HNOCS simulator. I extended this simulator to model new architectures of routers. More details on this work are available [here](https://dl.acm.org/doi/10.1145/3529106).
